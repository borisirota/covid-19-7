var locales = {
	"en-US": {
		"page-title": "Is it Coronavirus?",
		"page-subtitle1": "Coronavirus vs Flu vs Common Cold vs Allergies",
		"table-title": "Select symptoms to highlight related diseases",
		"disclaimer": '<p></p><div><strong>DISCLAIMER:</strong> THIS IS NOT A MEDICAL DIAGNOSTIC TOOL. ALWAYS CHECK WITH YOUR DOCTOR IF YOU HAVE ANY SYMPTOMS!</div><div style="margin-top:10px">This visualization is intended to help you understand the overwhelming flood of information about possible coronavirus symptoms. It shows the likelihood of symptoms being caused by one of 4 conditions. Your symptoms might be caused by a different condition not included in the tool.</div><p id="disclaimer-agree-wrapper"><button id="disclaimer-agree">I Understand and Agree</button></p><p></p>',
		"symptoms-title": "Your Symptoms",
		"symptom": "Symptom",
		"diseases": {
			"covid-19": "COVID-19",
			"common cold": "Common Cold",
			"flu": "Flu",
			"allergies": "Allergies"
		},
		"symptoms": {
			"fever": "Fever",
			"shortness of breath": "Shortness of breath",
			"itchy or watery eyes": "Itchy or watery eyes",
			"dry cough": "Dry cough",
			"headaches": "Headaches",
			"aches and pains": "Aches and pains",
			"sore throat": "Sore throat",
			"fatigue": "Fatigue",
			"diarrhea": "Diarrhea",
			"runny or stuffy nose": "Runny or stuffy nose",
			"sneezing": "Sneezing",
			"vomiting": "Vomiting",
			"worsening symptoms": "Worsening symptoms",
			"history of travel": "History of travel",
			"exposure to known covid-19 patient": "Exposure to known<br/> COVID-19 patient"
		},
		"frequencies": {
			"common": "Common",
			"rare": "Rare",
			"no": "No",
			"sometimes": "Sometimes",
			"mild": "Mild",
		},
		"cta": "<ul id='cta-list'><li>Interested in a customized version for your region with coverage of the local test labs?</li><li>Looking for telemedicine solutions for the COVID-19 crisis and continuity of care for survivors with residual damage of lungs, heart and other organs?</li><li>Have feedback and ideas - including real data to improve the data model?</li></ul><p id='cta-button-wrapper'><a id='cta-button' href='https://beecardia.com/contact' target='_blank'>Contact Us</a></p>",
		"tech-limits": "<p><strong>Technology and limitations:</strong></p><p>Currently, we do not have access to a sufficiently large corpus of data to build a realistic model for accurate assessment of disease likelihood from symptoms. Instead, we use a coarse approximation based on Bayesian inference, assuming independence of symptoms. The likelihood of symptoms given disease are based on data collected from different publications by CDC, NIH, WHO, Asthma and Allergy Foundation of America and other sources.</p><p>Our team has many years of experience in machine learning, AI and medical triage and back in 2011 introduced the concept of <a href='https://en.wikipedia.org/wiki/Computer-aided_simple_triage' target='_blank'>computer-aided simple triage (CAST)</a> while developing medical imaging solutions within our sister company <a href='http://rcadia.com/' target='_blank'>Rcadia</a>.</p>",
		"about-us": "About Us",
		"terms-of-use": "Terms of Use",
		"privacy-policy": "Privacy Policy",
		"copyrights": "Copyrights",
		"contact-us": "Contact Us"
	}
}
